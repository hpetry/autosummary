OIFS="$IFS"
IFS=$'\n'

for i in {1..60}
do
    echo -n "$i "
    head -n 1 salesdata.csv | awk -F "\"*,\"*" -v j="$i" '{print $j}'
done

rm public/index2.html
echo "<html>" >> public/index2.html
echo "  <head>" >> public/index2.html
echo "\
    <style> \n\
      table, th, td { \n\
        border: 1px solid black; \n\
        border-collapse: collapse; \n\
        padding: 15px; \n\
      } \n\
   </style>" >> public/index2.html
echo "  </head>" >> public/index2.html
echo "  <body>" >> public/index2.html
echo "    <table style=\"width:100%\">" >> public/index2.html

for i in $(
              grep -i insurance salesdata.csv |
              awk -F "\"*,\"*"  'BEGIN {OFS=","} {if ($17 == "NORMAL WEAR" || $17 == "MINOR DENT/SCRATCHES") print }' | # normal wear and minor damage
              awk -F "\"*,\"*"  'BEGIN {OFS=","} {if ($20 == "CT") print }' | # clean title
              awk -F "\"*,\"*"  'BEGIN {OFS=","} {if ($10 == "V") print }' | # vehicle type
              awk -F "\"*,\"*"  '{if ($35 < 500) print }' | #min bid less than 500
              awk -F "\"*,\"*"  '{if ($24 < 200000) print }' | #not more than $200k miles
              awk -F "\"*,\"*"  '{if ($11 >= 2008) print }' | #not too old
              awk -F "\"*,\"*"  'BEGIN {OFS=","} {if ($38 == "MI" || $38 == "IL" || $38 == "IN" || $38 == "WI" || $38 == "OH" || $38 == "MO" || $38 == "KY" || $38 == "TN" || $38 == "IA") print }' #midwest states
          )
do
    # printf '%s\n' $i
    lot=$(printf "%s\n" $i | awk -F "\"*,\"*"  '{print $9}')
    make=$(printf "%s\n" $i | awk -F "\"*,\"*"  '{print $12}')
    model=$(printf "%s\n" $i | awk -F "\"*,\"*"  '{print $13}')
    year=$(printf "%s\n" $i | awk -F "\"*,\"*"  '{print $11}')
    echo $lot
    echo "      <tr>" >> public/index2.html
    echo "        <td>" >> public/index2.html
    echo "          $make" >> public/index2.html
    echo "        </td>" >> public/index2.html
    echo "        <td>" >> public/index2.html
    echo "          $model" >> public/index2.html
    echo "        </td>" >> public/index2.html
    echo "        <td>" >> public/index2.html
    echo "         $year" >> public/index2.html
    echo "        </td>" >> public/index2.html
    echo "        <td>" >> public/index2.html
    echo "          <a href=\"https://copart.com/lot/$lot\" target=\"_blank\">$lot</a>" >> public/index2.html
    echo "        </td>" >> public/index2.html
    echo "        <td>" >> public/index2.html
    echo "          <a href=\"/$lot/appraisal.html\" target=\"_blank\">appraisal</a>" >> public/index2.html
    echo "        </td>" >> public/index2.html
    echo "      </tr>" >> public/index2.html
    python3 ./fetch.py $lot
    python3 ./appraise.py $lot
    mv $lot public
done

echo "    </table>" >> public/index2.html
echo "  </body>" >> public/index2.html
echo "</html>" >> public/index2.html

# grep -i rental salesdata.csv | awk -F "\"*,\"*"  '{print $52}' | sort | uniq -c | sort -n -r
# grep -i insurance salesdata.csv | awk -F "\"*,\"*"  '{print $52}' | sort | uniq -c | sort -n -r
