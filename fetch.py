import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
import urllib
import time 
import os
import sys

if len(sys.argv) != 2:
    print("usage: python3 ./autofetch.py {LOT NUMBER}")
    exit

# lot = "76558553"
# lot = "45895064"
# lot = "78835063"
# lot = "40025294"
# lot = "41676354"
lot = sys.argv[1]
try:
    os.mkdir(lot)
except:
    print("Directory '"+lot+"' already exists")

driver = webdriver.Chrome()
# driver.get("https://google.co.in")
# driver.get("https://www.copart.com/lot/76558553/Photos/2023-subaru-outback-limited-ca-van-nuys")
driver.get("https://www.copart.com/lot/"+lot)

get_url = str(driver.current_url)

url_parts = get_url.split(lot)

new_url = url_parts[0]+lot+"/Photos"+url_parts[1]

driver.get(new_url)

print(new_url)
time.sleep(10)

imgResults = driver.find_elements(By.XPATH,"//img[contains(@class,'img-responsive')]")

src = []
for img in imgResults:
    src.append(img.get_attribute('src'))

imgnum = 0
for i in range(0,len(imgResults)):
    if ".jpg" in str(src[i]):
        urllib.request.urlretrieve(str(src[i]),"./"+lot+"/vehicle{}.jpg".format(imgnum))
        imgnum = imgnum + 1

# //*[@id="lot-details-bottom"]/div[1]


# details = driver.find_element(By.XPATH,"//*[@id='lot-details-bottom']/div[1]").get_attribute('outerHTML')

details = driver.find_element(By.XPATH,"//*[@id='lot-details-bottom']/div[1]/div/div/div[2]").get_attribute('outerHTML')

# //*[@id="lot-details-bottom"]/div[1]/div/div/div[2]

# print("HTML code:\n",details)

file1 = open(lot+"/details.html", "w")
file1.write(details)
file1.close() 



# //*[@id="sale-information-block"]

saleinfo = driver.find_element(By.XPATH,"//*[@id='sale-information-block']").get_attribute('outerHTML')
file1 = open(lot+"/saleinfo.html", "w")
file1.write(saleinfo)
file1.close() 


# //*[@id="lot-details"]/div/div[1]/div/div/div/div/div/h1
description = driver.find_element(By.XPATH,"//*[@id='lot-details']/div/div[1]/div/div/div/div/div/h1").get_attribute('outerHTML')
file1 = open(lot+"/description.html", "w")
file1.write(description)
file1.close() 

# /html/body/section[1]/div/div[4]/div

try:

  driver.get("https://bid.cars/lot/1-"+lot)
  a = driver.find_element(By.CLASS_NAME,"link-history")
  a.click()
  time.sleep(2)
  # //*[@id="content"]/div/div[4]/div
  # history = driver.find_element(By.XPATH,"//*[@id='content']/div/div[4]/div").get_attribute('outerHTML')
  history = driver.find_element(By.CLASS_NAME,"sales-history").get_attribute('outerHTML')
  file1 = open(lot+"/history.html", "w")
  file1.write(history)
  file1.close() 
  time.sleep(2)

except:
  file1 = open(lot+"/history.html", "w")
  file1.write("No bid.cars history available")
  file1.close()
  
