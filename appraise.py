import base64
import requests
import os

import sys

if len(sys.argv) != 2:
    print("usage: python3 ./appraise.py {LOT NUMBER}")
    exit

lot = sys.argv[1]

# OpenAI API Key
api_key = "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"

headers = {
    "Content-Type": "application/json",
    "Authorization": f"Bearer {api_key}"
}

instructions = "The images depict different angles and views of the same vehicle. Please provide a description of the vehicle that is detailed enough for an appraisal identifying any damge or repairs needed. Use comparisons against online photos of the vehicle to assess damage versus body styling. Any open windows should be assumed to be broken/missing glass."

# Function to encode the image
def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')

files = os.listdir(lot)
base64_images = []

file1 = open(lot+"/description.html", "r")
description = file1.read()
file1.close()

file1 = open(lot+"/history.html", "r")
history = file1.read()
file1.close()

files = ["vehicle0.jpg","vehicle1.jpg","vehicle2.jpg"]

for f in files:
    if ".jpg" in f:
        print(lot+"/"+f)
        base64_images.append(encode_image(lot+"/"+f))

# print(base64_images)
# Path to your image

content = []
content.append({
            "type": "text",
            "text": instructions
          })

content.append({
            "type": "text",
            "text": f"Type and year of vehicle: {description}"
          })


for d in base64_images:
    content.append({
            "type": "image_url",
            "image_url": {
              "url": f"data:image/jpeg;base64,{d}"
            }
          })

# Getting the base64 string

payload = {
    "model": "gpt-4-vision-preview",
    "messages": [
      {
        "role": "user",
        "content": content
      }
    ],
    "max_tokens": 300
}

response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)
# print(response.json())

condition1 = response.json()['choices'][0]['message']['content']
# print(condition)


files = ["vehicle3.jpg","vehicle4.jpg","vehicle5.jpg"]

for f in files:
    if ".jpg" in f:
        print(lot+"/"+f)
        base64_images.append(encode_image(lot+"/"+f))

# print(base64_images)
# Path to your image

content = []
content.append({
            "type": "text",
            "text": instructions
          })

content.append({
            "type": "text",
            "text": f"Type and year of vehicle: {description}"
          })

for d in base64_images:
    content.append({
            "type": "image_url",
            "image_url": {
              "url": f"data:image/jpeg;base64,{d}"
            }
          })

# Getting the base64 string

payload = {
    "model": "gpt-4-vision-preview",
    "messages": [
      {
        "role": "user",
        "content": content
      }
    ],
    "max_tokens": 300
}

response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)
# print(response.json())

condition2 = response.json()['choices'][0]['message']['content']
# print(condition)

files = ["vehicle6.jpg","vehicle7.jpg","vehicle8.jpg"]

for f in files:
    if ".jpg" in f:
        print(lot+"/"+f)
        base64_images.append(encode_image(lot+"/"+f))

# print(base64_images)
# Path to your image

content = []
content.append({
            "type": "text",
            "text": instructions
          })

content.append({
            "type": "text",
            "text": f"Type and year of vehicle: {description}"
          })


for d in base64_images:
    content.append({
            "type": "image_url",
            "image_url": {
              "url": f"data:image/jpeg;base64,{d}"
            }
          })

# Getting the base64 string

payload = {
    "model": "gpt-4-vision-preview",
    "messages": [
      {
        "role": "user",
        "content": content
      }
    ],
    "max_tokens": 300
}

response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)
# print(response.json())

condition3 = response.json()['choices'][0]['message']['content']
# print(condition)



file1 = open(lot+"/details.html", "r")
details = file1.read()
file1.close()
# print(details)

payload = {
    "model": "gpt-4",
    "messages": [
      {
        "role": "user",
        "content": [
          {
            "type": "text",
            "text": "Included is a text/html description of a vehicle including the make, model, year and general condition. Please generate an appraisal of the vehicle based on those factors and anything else that you find in the text or online. If a zero damage dollar amount is not provided, use the internet to look up an approximate value for the car in the condition described."
          },
          {
            "type": "text",
            "text": f"Type and year of vehicle: {description}"
          },
          {
            "type": "text",
            "text": f"Other details: {details}"
          },
          {
            "type": "text",
            "text": f"Condition of the vehicle: {condition1}"
          },
          {
            "type": "text",
            "text": f"Condition of the vehicle continued: {condition2}"
          },
          {
            "type": "text",
            "text": f"Condition of the vehicle continued: {condition3}"
          }
        ]
      }
    ],
    "max_tokens": 300
}

response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)

appraisal = response.json()['choices'][0]['message']['content']
print(appraisal)


# 

payload = {
    "model": "gpt-4",
    "messages": [
      {
        "role": "user",
        "content": [
          {
            "type": "text",
            "text": "Please complete the appraisal with an HTML table showing the zero damage estimate, any considerations that lower the value, including $1000 for transportation and then a total appraised value at the bottom. If a zero damage dollar amount is not provided, use the internet to look up an approximate value for the car in the condition described."
          },
          {
            "type": "text",
            "text": f"Vehicle appraisal: {appraisal}"
          },
          {
            "type": "text",
            "text": f"Other details: {details}"
          },
          {
            "type": "text",
            "text": f"Condition of the vehicle: {description}"
          }
        ]
      }
    ],
    "max_tokens": 300
}

response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)

table = response.json()['choices'][0]['message']['content']
print(table)


valuetext = "<html><body>"
valuetext = valuetext + description
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<h1>Sales history</h1>\n"
valuetext = valuetext + history
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<h1>Guess at Value</h1>\n"
valuetext = valuetext + table
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<h1>Assessment of vehicle</h1>\n"
valuetext = valuetext + appraisal
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<h1>Vehicle Details</h1>\n"
valuetext = valuetext + details
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<h1>Condition</h1>\n"
valuetext = valuetext + condition1
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + condition2
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + condition3
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<br>\n"
valuetext = valuetext + "<h1>Photos</h1>\n"

files = os.listdir(lot)
n=0
for f in files:
    if ".jpg" in f:
        valuetext = valuetext + f"<img src='vehicle{n}.jpg'></img>"
        valuetext = valuetext + "<br>\n"
        n = n + 1
valuetext = valuetext + "</body></html>"


file1 = open(lot+"/appraisal.html", "w")
file1.write(valuetext)
file1.close()
